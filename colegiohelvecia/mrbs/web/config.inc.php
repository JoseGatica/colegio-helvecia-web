<?php
namespace MRBS;

// $Id$

/**************************************************************************
 *   MRBS Configuration File
 *   Configure this file for your site.
 *   You shouldn't have to modify anything outside this file.
 *
 *   This file has already been populated with the minimum set of configuration
 *   variables that you will need to change to get your system up and running.
 *   If you want to change any of the other settings in systemdefaults.inc.php
 *   or areadefaults.inc.php, then copy the relevant lines into this file
 *   and edit them here.   This file will override the default settings and
 *   when you upgrade to a new version of MRBS the config file is preserved.
 **************************************************************************/

/**********
 * Timezone
 **********/
 
// The timezone your meeting rooms run in. It is especially important
// to set this if you're using PHP 5 on Linux. In this configuration
// if you don't, meetings in a different DST than you are currently
// in are offset by the DST offset incorrectly.
//
// Note that timezones can be set on a per-area basis, so strictly speaking this
// setting should be in areadefaults.inc.php, but as it is so important to set
// the right timezone it is included here.
//
// When upgrading an existing installation, this should be set to the
// timezone the web server runs in.  See the INSTALL document for more information.
//
// A list of valid timezones can be found at http://php.net/manual/timezones.php
// The following line must be uncommented by removing the '//' at the beginning
$timezone = "America/Santiago";


/*******************
 * Database settings
 ******************/
// Which database system: "pgsql"=PostgreSQL, "mysql"=MySQL
$dbsys = "mysql";
// Hostname of database server. For pgsql, can use "" instead of localhost
// to use Unix Domain Sockets instead of TCP/IP. For mysql "localhost"
// tells the system to use Unix Domain Sockets, and $db_port will be ignored;
// if you want to force TCP connection you can use "127.0.0.1".
$db_host = "localhost";
// If you need to use a non standard port for the database connection you
// can uncomment the following line and specify the port number
// $db_port = 1234;
// Database name:
$db_database = "colegioh_mrbs";
// Schema name.  This only applies to PostgreSQL and is only necessary if you have more
// than one schema in your database and also you are using the same MRBS table names in
// multiple schemas.
//$db_schema = "public";
// Database login user name:
$db_login = "colegioh";
// Database login password:
$db_password = 'sodeval12185';
// Prefix for table names.  This will allow multiple installations where only
// one database is available
$db_tbl_prefix = "mrbs_";
// Set $db_persist to TRUE to use PHP persistent (pooled) database connections.  Note
// that persistent connections are not recommended unless your system suffers significant
// performance problems without them.   They can cause problems with transactions and
// locks (see http://php.net/manual/en/features.persistent-connections.php) and although
// MRBS tries to avoid those problems, it is generally better not to use persistent
// connections if you can.
// $db_persist = FALSE;


/* Add lines from systemdefaults.inc.php and areadefaults.inc.php below here
   to change the default configuration. Do _NOT_ modify systemdefaults.inc.php
   or areadefaults.inc.php.  */

/*Asistentes*/
$auth["admin"][] = "colegioh_joseg";
$auth["admin"][] = "josegatica";
$auth["session"] = "php";
$auth["type"] = "config";
$auth["user"]["colegioh_joseg"] = "123DreamTheater";
$auth["user"]["josegatica"] = "123DreamTheater";
$auth["user"]["claudiohenriquez"] = "claudiohenriquez";
$auth["user"]["guidofigueroa"] = "gfigueroa502";
$auth["user"]["danielasanchez"] = "dsanchez513";
$auth["user"]["juanmuñoz"] = "jmuñoz524";
$auth["user"]["giselamuñoz"] = "gmuñoz535";
$auth["user"]["claudionovoa"] = "cnovoa546";
$auth["user"]["barbaraparada"] = "bparada557";
$auth["user"]["pilarhott"] = "phott568";
$auth["user"]["angelaoliva"] = "aoliva579";
$auth["user"]["ernacarrasco"] = "ecarrasco580";
$auth["user"]["mariavidal"] = "mvidal591";
$auth["user"]["hugovera"] = "hvera602";
$auth["user"]["osvaldobenavides"] = "obenavides613";
$auth["user"]["fredymartinez"] = "fmartinez624";
$auth["user"]["normaojeda"] = "nojeda635";
$auth["user"]["audomiragonzalez"] = "agonzalez646";
$auth["user"]["luisvega"] = "lvega657";
$auth["user"]["ceciliacalderon"] = "ccalderon668";
$auth["user"]["vivianaurrutia"] = "vurrutia679";
$auth["user"]["gloriamendoza"] = "gmendoza680";
$auth["user"]["juancarlossaez"] = "jcsaez691";
$auth["user"]["mirtalape���a"] = "mpe���a702";
$auth["user"]["dagobertocarrasco"] = "dcarrasco713";
$auth["user"]["ingridrios"] = "irios724";
$auth["user"]["juancarloslopez"] = "jclopez735";
$auth["user"]["ruthmartinez"] = "rmartinez746";
$auth["user"]["pamelatrujillo"] = "ptrujillo757";
$auth["user"]["nicolemu���oz"] = "nmu���oz768";
$auth["user"]["pabloortega"] = "portega779";

/*Docentes*/
$auth["user"]["jacquelinevera"] = "jvera357";
$auth["user"]["jacquelinevargas"] = "jvargas468";
$auth["user"]["claudiamartinez"] = "cmartinez579";
$auth["user"]["paolavera"] = "pvera680";
$auth["user"]["robertohuenulef"] = "rhuenulef791";
$auth["user"]["mariaferhernandez"] = "mfhernandez824";
$auth["user"]["yessicaalarcon"] = "yalarcon913";
$auth["user"]["marcelocontreras"] = "mcontreras103";
$auth["user"]["jimenaschencke"] = "jschencke113";
$auth["user"]["angelicaperez"] = "aperez124";
$auth["user"]["maritzahernandez"] = "mhernandez135";
$auth["user"]["marcelofarias"] = "mfarias146";
$auth["user"]["ximenavega"] = "xvega157";
$auth["user"]["martamatamala"] = "mmatamala168";
$auth["user"]["adrianamiralles"] = "amiralles179";
$auth["user"]["walterarnaldi"] = "warnaldi180";
$auth["user"]["moisessandoval"] = "msandoval191";
$auth["user"]["felipeosses"] = "fosses202";
$auth["user"]["paulinahermosilla"] = "phermosilla213";
$auth["user"]["mariodiaz"] = "mdiaz224";
$auth["user"]["sebastianmariman"] = "smariman235";
$auth["user"]["katherinemancilla"] = "kmancilla246";
$auth["user"]["paolajara"] = "pjara257";
$auth["user"]["steffirios"] = "srios268";
$auth["user"]["exequielmujica"] = "emujica279";
$auth["user"]["carolinaquezada"] = "cquezada280";
$auth["user"]["claudiacastillo"] = "ccastillo291";
$auth["user"]["madelynlefno"] = "mlefno302";
$auth["user"]["antonioojeda"] = "aojeda313";
$auth["user"]["felipesilva"] = "fsilva324";
$auth["user"]["lissettetello"] = "ltello335";
$auth["user"]["lorenaesparza"] = "lesparza346";
$auth["user"]["katherinecanario"] = "kcanario357";
$auth["user"]["jacquelinehurtado"] = "jhurtado368";
$auth["user"]["michaelbrandt"] = "mbrandt379";
$auth["user"]["martagaray"] = "mgaray380";
$auth["user"]["blancaobando"] = "bobando391";
$auth["user"]["franciscomoreno"] = "fmoreno402";
$auth["user"]["erwinleal"] = "eleal413";
$auth["user"]["roxanaespinoza"] = "respinoza424";
$auth["user"]["rosafontanez"] = "rfontanez435";
$auth["user"]["marianelalara"] = "mlara446";
$auth["user"]["karinamartinez"] = "kmartinez457";
$auth["user"]["analuisacuevas"] = "acuevas468";

$mrbs_company = "Colegio Helvecia";
